﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Servidor
{
    public class StatusChangedEventArgs : EventArgs
    {
        private string EventMsg;

        public string EventMessage
        {
            get
            {
                return EventMsg;
            }
            set
            {
                EventMsg = value;
            }
        }

        // Constructor para configurar el mensaje
        public StatusChangedEventArgs(string strEventMsg)
        {
            EventMsg = strEventMsg;
        }
    }

    public delegate void StatusChangedEventHandler(object sender, StatusChangedEventArgs e);

    class ChatServer
    {
        // esta tabla hash guarda los usuarios y las conexiones
        public static Hashtable htUsers = new Hashtable(30); // limite de 30 usuarios al mismo tiempo
        public static Hashtable htConnections = new Hashtable(30);// limite de 30 usuarios al mismo tiempo 
        // guarda las ip que pasan
        private IPAddress ipAddress;
        private TcpClient tcpClient;
        // este evento y argumento notifica cuando un usuario se conecta, desconecta, envia mensaje, etc.
        public static event StatusChangedEventHandler StatusChanged;
        private static StatusChangedEventArgs e;

        public ChatServer(IPAddress address)
        {
            ipAddress = address;
        }

        // el proceso espera escuchando
        private Thread thrListener;

        // objeto tcp que espera conecciones
        private TcpListener tlsClient;

        bool ServRunning = false;

        // añade el usuario a la tabla hash
        public static void AddUser(TcpClient tcpUser, string strUsername)
        {
            ChatServer.htUsers.Add(strUsername, tcpUser);
            ChatServer.htConnections.Add(tcpUser, strUsername);

            // anuncia la nueva coneccion a todo el mundo
            SendAdminMessage(htConnections[tcpUser] + " Se ha unido a nosotros");
        }

        // elimina al usuario de la tabla hash
        public static void RemoveUser(TcpClient tcpUser)
        {
            // si el usuario esta aqui
            if (htConnections[tcpUser] != null)
            {
                //primero anuncia la informacion y luego dice a los demas la desconeccion
                SendAdminMessage(htConnections[tcpUser] + "Nos ha dejado");

                //elimina al usuario de la tabla 
                ChatServer.htUsers.Remove(ChatServer.htConnections[tcpUser]);
                ChatServer.htConnections.Remove(tcpUser);
            }
        }

        public static void OnStatusChanged(StatusChangedEventArgs e)
        {
            StatusChangedEventHandler statusHandler = StatusChanged;
            if (statusHandler != null)
            {
                statusHandler(null, e);
            }
        }

        // envia mensajes administrativos
        public static void SendAdminMessage(string Message)
        {
            StreamWriter swSenderSender;

            // primero que todo, dice quien dice que
            e = new StatusChangedEventArgs("Administrador: " + Message);
            OnStatusChanged(e);

            //crea un arreglo de clientes tcp
            TcpClient[] tcpClients = new TcpClient[ChatServer.htUsers.Count];
            //copia los objetos tcp en el arreglo
            ChatServer.htUsers.Values.CopyTo(tcpClients, 0);
            for (int i = 0; i < tcpClients.Length; i++)
            {
                // intenta mandar un mensaje a cada auno
                try
                {
                    //si el mensaje esta en blanco o la conexion es nula se saldra
                    if (Message.Trim() == "" || tcpClients[i] == null)
                    {
                        continue;
                    }
                    // Send the message to the current user in the loop
                    swSenderSender = new StreamWriter(tcpClients[i].GetStream());
                    swSenderSender.WriteLine("Administrador: " + Message);
                    swSenderSender.Flush();
                    swSenderSender = null;
                }
                catch //si hay un problema o no esta el usuario lo elimina
                {
                    RemoveUser(tcpClients[i]);
                }
            }
        }

        // envia mensajes de un usuario a los demas
        public static void SendMessage(string From, string Message)
        {
            StreamWriter swSenderSender;

            // dice en la aplicacion quien dice que
            e = new StatusChangedEventArgs(From + " Dice: " + Message);
            OnStatusChanged(e);

            //crea un arreglo de clientes tcp
            TcpClient[] tcpClients = new TcpClient[ChatServer.htUsers.Count];
            //copea los objetos tcp en el arreglo
            ChatServer.htUsers.Values.CopyTo(tcpClients, 0);
            for (int i = 0; i < tcpClients.Length; i++)
            {
                //intenta mandar un mensaje a cada uno
                try
                {
                    // si el mensaje esta en blanco o la conexion es nula se saldra
                    if (Message.Trim() == "" || tcpClients[i] == null)
                    {
                        continue;
                    }
                    // envia el mensaje al usuario en el loop
                    swSenderSender = new StreamWriter(tcpClients[i].GetStream());
                    swSenderSender.WriteLine(From + " Dice: " + Message);
                    swSenderSender.Flush();
                    swSenderSender = null;
                }
                catch //si hay un problema o no esta el usuario se saldra
                {
                    RemoveUser(tcpClients[i]);
                }
            }
        }

        public void StartListening()
        {

            //obtiene la ip del primer dispositivo de la red 
            IPAddress ipaLocal = ipAddress;

            //crea el objeto que escucha con la ip del servidor y el puerto
            tlsClient = new TcpListener(1986);

            //inicia el TCP y espera por conecciones
            tlsClient.Start();

            //el loop checa si es verdad antes de esperar conecciones
            ServRunning = true;

            //inicia el nuevo proceso de si hay host escuchando
            thrListener = new Thread(KeepListening);
            thrListener.Start();
        }

        private void KeepListening()
        {
            //mientras el server esta corriendo
            while (ServRunning == true)
            {
                //acepta una coneccion pendiente
                tcpClient = tlsClient.AcceptTcpClient();
                //crea una nueva instancia de conecciones
                Connection newConnection = new Connection(tcpClient);
            }
        }
    }

    //esta clase mantiene conecciones
    class Connection
    {
        TcpClient tcpClient;
        //el proceso enviara informacion al cliente
        private Thread thrSender;
        private StreamReader srReceiver;
        private StreamWriter swSender;
        private string currUser;
        private string strResponse;

        //el constructor de la clase toma en una coneccion tcp
        public Connection(TcpClient tcpCon)
        {
            tcpClient = tcpCon;
            //el proceso acepta al cliente y espera por mensajes
            thrSender = new Thread(AcceptClient);
            //el proceso llama al metodo AcceptClient()
            thrSender.Start();
        }

        private void CloseConnection()
        {
            //cierra el objeto abierto
            tcpClient.Close();
            srReceiver.Close();
            swSender.Close();
        }

        //ocurre cuando un nuevo cliente es aceptado
        private void AcceptClient()
        {
            srReceiver = new System.IO.StreamReader(tcpClient.GetStream());
            swSender = new System.IO.StreamWriter(tcpClient.GetStream());

            //lee la informacion de la cuenta del cliente
            currUser = srReceiver.ReadLine();

            //respuesta del cliente
            if (currUser != "")
            {
                //guarda el nombre de usuario en la tabla hash
                if (ChatServer.htUsers.Contains(currUser) == true)
                {
                    // 0 significa no conectado
                    swSender.WriteLine("0|este usuario ya existe.");
                    swSender.Flush();
                    CloseConnection();
                    return;
                }
                else if (currUser == "Administrador")
                {
                    // 0 significa no conectado
                    swSender.WriteLine("0|Este nombre esta reservado.");
                    swSender.Flush();
                    CloseConnection();
                    return;
                }
                else
                {
                    // 1 significa conectado correctamente
                    swSender.WriteLine("1");
                    swSender.Flush();

                    //añade el usuario a la tabla hash e inicia a escuchar mensajes de el
                    ChatServer.AddUser(tcpClient, currUser);
                }
            }
            else
            {
                CloseConnection();
                return;
            }

            try
            {
                //se mantiene esperando por un mensaje del usuario
                while ((strResponse = srReceiver.ReadLine()) != "")
                {
                    //si es invalido, remueve al usuario
                    if (strResponse == null)
                    {
                        ChatServer.RemoveUser(tcpClient);
                    }
                    else
                    {
                        //de otor modo envia el mensaje a los demas usuarios
                        ChatServer.SendMessage(currUser, strResponse);
                    }
                }
            }
            catch
            {
                //si algo sale mal con el usuario, lo remueve
                ChatServer.RemoveUser(tcpClient);
            }
        }
    }
}

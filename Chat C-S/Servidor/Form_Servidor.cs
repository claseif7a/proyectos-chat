﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Servidor
{
    public partial class Form_Servidor : Form
    {
        private delegate void UpdateStatusCallback(string strMessage);

        public Form_Servidor()
        {
            InitializeComponent();
        }

        private void btnListen_Click(object sender, EventArgs e)
        {
            //convierte la ip del server de la textbox
            IPAddress ipAddr = IPAddress.Parse(txtIp.Text);
            //crea una nueva instacia del objeto del chat
            ChatServer mainServer = new ChatServer(ipAddr);
            ChatServer.StatusChanged += new StatusChangedEventHandler(mainServer_StatusChanged);
            // empieza a escuchar conexiones
            mainServer.StartListening();
            // dice que a empezado a esperar conexiones
            txtLog.AppendText("Esperando conexiones...\r\n");
            btnCambiar.Enabled = false;
        }

        public void mainServer_StatusChanged(object sender, StatusChangedEventArgs e)
        {
            // llama al metodo que actualiza la forma
            this.Invoke(new UpdateStatusCallback(this.UpdateStatus), new object[] { e.EventMessage });
        }

        private void UpdateStatus(string strMessage)
        {
            // actualiza el registro con el mensaje
            txtLog.AppendText(strMessage + "\r\n");
        }

        private void btnCambiar_Click(object sender, EventArgs e)
        {
            txtIp.Enabled = true;
        }
    }
}

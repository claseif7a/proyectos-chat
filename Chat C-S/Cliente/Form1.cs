﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cliente
{
    public partial class Form_Cliente : Form
    {
        // mantendra el nombre de usuario
        private string UserName = "Desconocido";
        private StreamWriter swSender;
        private StreamReader srReceiver;
        private TcpClient tcpServer;
        // necesario para actualizar la forma con los mensajes de otro proceso
        private delegate void UpdateLogCallback(string strMessage);
        // necesario para cambiar la forma a desconecado de otro proceso
        private delegate void CloseConnectionCallback(string strReason);
        private Thread thrMessaging;
        private IPAddress ipAddr;
        private bool Connected;

        public Form_Cliente()
        {
            Application.ApplicationExit += new EventHandler(OnApplicationExit);
            InitializeComponent();
        }

        public void OnApplicationExit(object sender, EventArgs e)
        {
            //si no estamos conectados pero esperando para conectar
            if (Connected == true)
            {
                Connected = false;
                swSender.Close();
                srReceiver.Close();
                tcpServer.Close();
            }
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (Connected == false)
            {
                InitializeConnection();
            }
            else
            {
                CloseConnection("Desconectado por el usuario");
            }
        }

        private void InitializeConnection()
        {
            //convirte la ip de la textbox en un objeto de direccionip
            ipAddr = IPAddress.Parse(txtIp.Text);
            //inicia una nueva conexion tcp al servidor de chat
            tcpServer = new TcpClient();
            tcpServer.Connect(ipAddr, 1986);
            //ayuda a saber si estan conectados o no
            Connected = true;
            UserName = txtUser.Text;
            //habilita y deshabilita los campos necesarios
            txtIp.Enabled = false;
            txtUser.Enabled = false;
            txtMessage.Enabled = true;
            btnSend.Enabled = true;
            btnConnect.Text = "Desconectado";
            //envia los nombres deseados al servidor
            swSender = new StreamWriter(tcpServer.GetStream());
            swSender.WriteLine(txtUser.Text);
            swSender.Flush();
            //inicia el proceso de recibir mensajes y conexiones futuras
            thrMessaging = new Thread(new ThreadStart(ReceiveMessages));
            thrMessaging.Start();
        }

        private void ReceiveMessages()
        {
            //recibe la respuesta del servidor
            srReceiver = new StreamReader(tcpServer.GetStream());
            // si el primer caracter de la respuesta es 1 la conexion es exitosa
            string ConResponse = srReceiver.ReadLine();
            if (ConResponse[0] == '1')
            {
                this.Invoke(new UpdateLogCallback(this.UpdateLog), new object[] { "Conectado exitosamente!" });
            }
            else //si el primer caracter no es 1 la conexion fallo
            {
                string Reason = "No conectado: ";
                Reason += ConResponse.Substring(2, ConResponse.Length - 2);
                this.Invoke(new CloseConnectionCallback(this.CloseConnection), new object[] { Reason });
                return;
            }
            // conectado y esperando, lee las lineas entrantes del servidor
            while (Connected)
            {

                this.Invoke(new UpdateLogCallback(this.UpdateLog), new object[] { srReceiver.ReadLine() });
            }
        }
        //este metodo es llamado de un proceso diferente
        private void UpdateLog(string strMessage)
        {
            txtLog.AppendText(strMessage + "\r\n");
        }

        private void CloseConnection(string Reason)
        {
            //habilita y dehabilita los campos necesarios
            txtLog.AppendText(Reason + "\r\n");
            txtIp.Enabled = true;
            txtUser.Enabled = true;
            txtMessage.Enabled = false;
            btnSend.Enabled = false;
            btnConnect.Text = "Conectar";

            Connected = false;
            swSender.Close();
            srReceiver.Close();
            tcpServer.Close();
        }

        private void SendMessage()
        {
            if (txtMessage.Lines.Length >= 1)
            {
                swSender.WriteLine(txtMessage.Text);
                swSender.Flush();
                txtMessage.Lines = null;
            }
            txtMessage.Text = "";
        }

        private void btnSend_Click_1(object sender, EventArgs e)
        {
            SendMessage();
        }

        private void txtMessage_TextChanged(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                SendMessage();
            }

        }
    }
}
